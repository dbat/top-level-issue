Top Level Issue
=

Is this an expected effect? It seems odd that the in-editor view looks different to when it's running.

![Weird offset](issue.png)

